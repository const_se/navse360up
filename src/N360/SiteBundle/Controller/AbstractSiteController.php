<?php

namespace N360\SiteBundle\Controller;

use GeoIp2\WebService\Client;
use N360\SystemBundle\Controller\AbstractInitializableController;
use N360\SystemBundle\Entity\City;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractSiteController extends AbstractInitializableController
{
    /** @var City */
    protected $city;
    /** @var string */
    protected $cityDomain;
    /** @var string */
    protected $countryDomain;
    /** @var bool */
    protected $firstVisit;

    public function initialize(Request $request)
    {
        parent::initialize($request);

        $this->city = null;
        $this->cityDomain = null;
        $this->countryDomain = null;
        $this->firstVisit = $this->request->cookies->has('main.city');

        $this->specifyCity();
    }

    /**
     * @return bool
     */
    public function isBotVisit()
    {
        $userAgent = strtolower($this->request->headers->get('User-Agent'));
        $isBot = false;

        foreach ($this->container->getParameter('bot_user_agents') as $engine)
            if (strpos($userAgent, strtolower($engine)) !== false) {
                $isBot = true;

                break;
            }

        return $isBot;
    }

    /**
     * @param bool $withSubdomain
     * @param string $route
     * @param array $parameters
     * @return bool|RedirectResponse
     */
    public function specifyRedirect($withSubdomain, $route, array $parameters = array())
    {
        if ($withSubdomain) {
            $needRedirect = is_null($this->cityDomain)
                || ($this->cityDomain !== $this->city->getDomain())
                || ($this->countryDomain !== $this->city->getCountry()->getDomain());

            if ($needRedirect) {
                $correctUrl = sprintf(
                    '%s://%s.%s%s',
                    $this->request->getScheme(),
                    $this->city->getDomain(),
                    $this->city->getCountry()->getDomain(),
                    $this->generateUrl($route, $parameters)
                );

                return $this->redirect($correctUrl, Response::HTTP_MOVED_PERMANENTLY);
            }
        }
        else {
            $needRedirect = ($this->countryDomain !== $this->city->getCountry()->getDomain())
                || !is_null($this->cityDomain);

            if ($needRedirect) {
                $correctUrl = sprintf(
                    '%s://%s%s',
                    $this->request->getScheme(),
                    $this->city->getCountry()->getDomain(),
                    $this->generateUrl($route, $parameters)
                );

                return $this->redirect($correctUrl, Response::HTTP_MOVED_PERMANENTLY);
            }
        }

        return false;
    }

    protected function specifyCity()
    {
        $this->specifyDomains();
        /** @var City $city */
        $city = null;

        if (!is_null($this->cityDomain)) $city = $this->specifyCityByDomains();

        if (is_null($city)) $city = $this->specifyCityByCookie();

        if (is_null($city)) {
            if ($this->isBotVisit()) {
                $this->logger->debug(sprintf(
                    'Attempt to specify the city by IP: Ignoring Search Bot (%s)',
                    $this->request->headers->get('User-Agent')
                ));
            }
            elseif ($this->request->getClientIp() !== '127.0.0.1') $city = $this->specifyCityByGeoIP();
        }

        if (is_null($city)) $city = $this->specifyDefaultCity();

        if (is_null($city)) throw $this->createNotFoundException('Not Found: can not specify the city');

        if (!is_null($city->getParent())) $city = $city->getParent();

        $this->city = $city;
        $this->cookies[] = new Cookie('main.city', $this->city->getId(), 0, '/', $this->domain);
    }

    protected function specifyCityByCookie()
    {
        return $this->getRepository('City')->createQueryBuilder('c')
            ->leftJoin('c.country', 'co')
            ->where('c.id = :id')
            ->andWhere('c.active = :active')
            ->andWhere('co.active = :active')
            ->setParameters(array('id' => $this->request->cookies->get('main.city', -1), 'active' => true))
            ->getQuery()->getOneOrNullResult();
    }

    protected function specifyCityByDomains()
    {
        return $this->getRepository('City')->createQueryBuilder('c')
            ->leftJoin('c.country', 'co')
            ->where('c.domain = :cityDomain')
            ->andWhere('co.domain = :countryDomain')
            ->andWhere('c.active = :active')
            ->andWhere('co.active = :active')
            ->setParameters(array(
                'cityDomain' => $this->cityDomain,
                'countryDomain' => $this->countryDomain,
                'active' => true
            ))->getQuery()->getOneOrNullResult();
    }

    protected function specifyCityByGeoIP()
    {
        $city = null;
        $geoip = new Client(
            $this->container->getParameter('geoip_userid'),
            $this->container->getParameter('geoip_licensekey'),
            $this->locale
        );

        try {
            $geoRecord = $geoip->city($this->request->getClientIp());
            $city = $this->getRepository('City')->createQueryBuilder('c')
                ->leftJoin('c.country', 'co')
                ->where('c.geoname = :geoname')
                ->andWhere('c.active = :active')
                ->andWhere('co.active = :active')
                ->setParameters(array('geoname' => $geoRecord->city->geonameId, 'active' => true))
                ->getQuery()->getOneOrNullResult();

            $this->logger->debug(sprintf(
                'Attempt to specify the city by IP: %s. IP: %s, city ID: %s.',
                is_null($city) ? 'failed' : 'success',
                $this->request->getClientIp(),
                is_null($city) ? 'null' : $city->getId()
            ));

            if (is_null($city)) {
                $d = 'acos(sin(latitude*pi()/180)*sin(:latitude*pi()/180)+cos(latitude*pi()/180)*cos(:latitude*pi()/180)*cos(longitude*pi()/180-:longitude*pi()/180))';
                $near = $this->getRepository('City')->createQueryBuilder('c')
                    ->select('c.id AS id, ' . $d . ' AS d')
                    ->leftJoin('c.country', 'co')
                    ->where('c.active = :active')
                    ->andWhere('co.active = :active')
                    ->setParameters(array(
                        'latitude' => $geoRecord->location->latitude,
                        'longitude' => $geoRecord->location->longitude,
                        'active' => true
                    ))->orderBy('d', 'ASC')
                    ->setMaxResults(1)
                    ->getQuery()->getArrayResult();

                if (count($near) > 0) $city = $this->getRepository('City')->find($d[0]['id']);
            }
        } catch (\Exception $e) {
            $this->logger->debug('Attempt to specify the city by IP: ' . $e->getMessage());
            $city = null;
        }

        return $city;
    }

    protected function specifyDefaultCity()
    {
        return $this->getRepository('City')->find($this->container->getParameter('default_city'));
    }

    protected function specifyDomains()
    {
        $host = explode('.', strtolower($this->request->getHost()));
        $count = count($host);

        if (($count < 2) || ($count > 3))
            throw $this->createNotFoundException(sprintf('Not Found: incorrect host %s', $this->request->getHost()));

        $this->countryDomain = implode('.', array_reverse(array_slice(array_reverse($host), 0, 2)));

        if ($this->countryDomain !== $this->domain)
            throw $this->createNotFoundException(
                sprintf('Not Found: incorrect country domain %s', $this->countryDomain)
            );

        $this->cityDomain = ($count == 3) ? $host[0] : null;
    }
}
