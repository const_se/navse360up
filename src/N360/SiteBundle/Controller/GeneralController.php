<?php

namespace N360\SiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration as Config;
use Symfony\Component\HttpFoundation\Response;

class GeneralController extends AbstractSiteController
{
    /**
     * @return Response
     * @Config\Route("/", name = "site_general_index")
     */
    public function indexAction()
    {
        return $this->render('N360SiteBundle:general:index.html.twig');
    }
}
