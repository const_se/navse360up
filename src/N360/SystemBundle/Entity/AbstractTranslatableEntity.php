<?php

namespace N360\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * Class AbstractTranslatableEntity
 * @package N360\SystemBundle\Entity
 * @ORM\MappedSuperclass
 */
abstract class AbstractTranslatableEntity extends AbstractEntity implements Translatable
{
    /**
     * @var string
     * @Gedmo\Locale
     */
    protected $locale;

    /**
     * @param $locale
     * @return $this
     */
    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }
}
