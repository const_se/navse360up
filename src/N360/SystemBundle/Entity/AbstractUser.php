<?php

namespace N360\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class AbstractUser
 * @package N360\SystemBundle\Entity
 * @ORM\MappedSuperclass
 */
abstract class AbstractUser extends AbstractEntity implements AdvancedUserInterface, EquatableInterface, \Serializable
{
    /**
     * @var string
     * @ORM\Column(name = "email", type = "string", nullable = true)
     */
    protected $email;

    /**
     * @var bool
     * @ORM\Column(name = "enabled", type = "boolean")
     */
    protected $enabled;

    /**
     * @var bool
     * @ORM\Column(name = "locked", type = "boolean")
     */
    protected $locked;

    /**
     * @var string
     * @ORM\Column(name = "name", type = "string", nullable = true)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(name = "password", type = "string")
     */
    protected $password;

    /**
     * @var string
     * @ORM\Column(name = "phone", type = "string", nullable = true)
     */
    protected $phone;

    /**
     * @var string
     * @ORM\Column(name = "salt", type = "string")
     */
    protected $salt;

    /**
     * @var string
     * @ORM\Column(name = "username", type = "string")
     */
    protected $username;

    /**
     * @var \DateTime
     * @ORM\Column(name = "visitedat", type = "datetime", nullable = true)
     */
    protected $visitedAt;

    public function __construct()
    {
        parent::__construct();

        $this->enabled = false;
        $this->locked = false;
        $this->salt = self::generateSalt();
    }

    public function eraseCredentials()
    {
        return $this;
    }

    /**
     * @return string
     */
    public static function generateSalt()
    {
        return openssl_random_pseudo_bytes(32);
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return \DateTime
     */
    public function getVisitedAt()
    {
        return $this->visitedAt;
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return !$this->locked;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->isEnabled();
    }

    public function isEqualTo(UserInterface $user)
    {
        return ($this->username === $user->getUsername())
            && ($this->password === $user->getPassword())
            && ($this->salt === $user->getSalt());
    }

    /**
     * @return bool
     */
    public function isLocked()
    {
        return $this->locked;
    }

    public function serialize()
    {
        return serialize(array(
            'id' => $this->id,
            'username' => $this->username,
            'password' => $this->password,
            'salt' => $this->salt
        ));
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param bool $enabled
     * @return $this
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @param bool $locked
     * @return $this
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @param string $salt
     * @return $this
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    public function unserialize($serialized)
    {
        $unserialized = unserialize($serialized);
        $this->id = $unserialized['id'];
        $this->username = $unserialized['username'];
        $this->password = $unserialized['password'];
        $this->salt = $unserialized['salt'];

        return $this;
    }

    /**
     * @return bool
     */
    public function updateVisit()
    {
        if (is_null($this->visitedAt)) $this->visitedAt = new \DateTime();

        $now = new \DateTime();
        $now->modify('-10 minutes');

        if ($this->visitedAt < $now) {
            $this->visitedAt = new \DateTime();

            return true;
        }

        return false;
    }
}
