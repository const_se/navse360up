<?php

namespace N360\SystemBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AdminUser
 * @package N360\SystemBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name = "adminusers")
 */
class AdminUser extends AbstractUser
{
    /**
     * @var ArrayCollection|Role[]
     * @ORM\ManyToMany(targetEntity = "N360\SystemBundle\Entity\Role")
     * @ORM\JoinTable(name = "adminuser_roles",
     *   joinColumns = {@ORM\JoinColumn(name = "userid", referencedColumnName = "id")},
     *   inverseJoinColumns = {@ORM\JoinColumn(name = "roleid", referencedColumnName = "id")}
     * )
     */
    protected $roles;

    public function __construct()
    {
        parent::__construct();

        $this->roles = new ArrayCollection();
    }

    /**
     * @return array|Role[]
     */
    public function getRoles()
    {
        return $this->roles->toArray();
    }

    /**
     * @return ArrayCollection|Role[]
     */
    public function getRolesCollection()
    {
        return $this->roles;
    }
}
