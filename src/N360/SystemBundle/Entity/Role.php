<?php

namespace N360\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * Class Role
 * @package N360\SystemBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name = "roles")
 */
class Role extends AbstractEntity implements RoleInterface
{
    const ADMIN = 'ROLE_ADMIN';
    const SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
    const USER = 'ROLE_USER';
    /**
     * @var string
     * @ORM\Column(name = "role", type = "string", unique = true)
     */
    protected $role;

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     * @return $this
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }
}
