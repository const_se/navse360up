<?php

namespace N360\SystemBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class City
 * @package N360\SystemBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name = "cities")
 */
class City extends AbstractTranslatableEntity
{
    /**
     * @var bool
     * @ORM\Column(name = "active", type = "boolean")
     */
    protected $active;

    /**
     * @var string
     * @ORM\Column(name = "caption", type = "string")
     */
    protected $caption;

    /**
     * @var Country
     * @ORM\ManyToOne(targetEntity = "N360\SystemBundle\Entity\Country", inversedBy = "cities")
     * @ORM\JoinColumn(name = "countryid", referencedColumnName = "id")
     */
    protected $country;

    /**
     * @var string
     * @ORM\Column(name = "domain", type = "string")
     */
    protected $domain;

    /**
     * @var int
     * @ORM\Column(name = "geonameid", type = "integer", unique = true)
     */
    protected $geoname;

    /**
     * @var City
     * @ORM\ManyToOne(targetEntity = "N360\SystemBundle\Entity\City", inversedBy = "subcities")
     * @ORM\JoinColumn(name = "parentid", referencedColumnName = "id", nullable = true)
     */
    protected $parent;

    /**
     * @var ArrayCollection|City[]
     * @ORM\OneToMany(targetEntity = "N360\SystemBundle\Entity\City", mappedBy = "parent")
     */
    protected $subcities;

    public function __construct()
    {
        parent::__construct();

        $this->active = true;
        $this->subcities = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @return int
     */
    public function getGeoname()
    {
        return $this->geoname;
    }

    /**
     * @return City
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return ArrayCollection|City[]
     */
    public function getSubcities()
    {
        return $this->subcities;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @param string $caption
     * @return $this
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * @param Country $country
     * @return $this
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @param string $domain
     * @return $this
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * @param int $geoname
     * @return $this
     */
    public function setGeoname($geoname)
    {
        $this->geoname = $geoname;

        return $this;
    }

    /**
     * @param City $parent
     * @return $this
     */
    public function setParent(City $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }
}
