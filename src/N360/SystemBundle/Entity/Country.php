<?php

namespace N360\SystemBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class Country
 * @package N360\SystemBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name = "countries")
 * @Gedmo\SoftDeleteable(fieldName = "deletedAt", timeAware = false)
 */
class Country extends AbstractTranslatableEntity
{
    /**
     * @var bool
     * @ORM\Column(name = "active", type = "boolean")
     */
    protected $active;

    /**
     * @var string
     * @ORM\Column(name = "caption", type = "string")
     */
    protected $caption;

    /**
     * @var ArrayCollection|City[]
     * @ORM\OneToMany(targetEntity = "N360\SystemBundle\Entity\City", mappedBy = "country")
     */
    protected $cities;

    /**
     * @var \DateTime|null
     * @ORM\Column(name = "deletedat", type = "datetime", nullable = true)
     */
    protected $deletedAt;

    /**
     * @var string
     * @ORM\Column(name = "domain", type = "string")
     */
    protected $domain;

    public function __construct()
    {
        parent::__construct();

        $this->active = true;
        $this->cities = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * @return ArrayCollection|City[]
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @param string $caption
     * @return $this
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * @param \DateTime $deletedAt
     * @return $this
     */
    public function setDeletedAt(\DateTime $deletedAt = null)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @param string $domain
     * @return $this
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }
}