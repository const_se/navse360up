<?php

namespace N360\SystemBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

abstract class AbstractEntityFormType extends AbstractFormType
{
    protected $dataClass;
    protected $validationGroups;

    public function __construct($name, $entity, $validation = array(), $method = self::METHOD_POST)
    {
        parent::__construct($name, $method);

        $this->dataClass = 'N360\SystemBundle\Entity\\' . $entity;
        $this->validationGroups = is_array($validation) ? $validation : array($validation);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->dataClass,
            'method' => $this->method,
            'validation_groups' => $this->validationGroups
        ));
    }
}
