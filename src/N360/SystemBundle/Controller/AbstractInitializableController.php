<?php

namespace N360\SystemBundle\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use N360\SystemBundle\Entity\AbstractUser;
use N360\SystemBundle\Entity\Role;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

abstract class AbstractInitializableController extends Controller
{
    /** @var AuthorizationChecker */
    protected $authChecker;
    /** @var array|Cookie[] */
    protected $cookies;
    /** @var string */
    protected $domain;
    /** @var string */
    protected $locale;
    /** @var Logger */
    protected $logger;
    /** @var EntityManager */
    protected $manager;
    /** @var array */
    protected $repositories;
    /** @var Request */
    protected $request;
    /** @var Session */
    protected $session;
    /** @var \DateTimeZone */
    protected $timezone;
    /** @var AdvancedUserInterface */
    protected $user;

    /**
     * @param string $entity
     * @return EntityRepository
     */
    public function getRepository($entity)
    {
        if (!array_key_exists($entity, $this->repositories))
            $this->repositories[$entity] = $this->manager->getRepository('N360SystemBundle:' . $entity);

        return $this->repositories[$entity];
    }

    public function initialize(Request $request)
    {
        $this->request = $request;

        $this->authChecker = $this->get('security.authorization_checker');
        $this->cookies = array();
        $this->domain = $this->container->getParameter('domain');
        $this->logger = $this->get('logger');
        $this->manager = $this->getDoctrine()->getManager();
        $this->repositories = array();
        $this->session = $this->request->getSession();
        $this->user = $this->getUser();

        $this->specifyLocale();
        $this->setTimezone(new \DateTimeZone($this->container->getParameter('default_timezone')));
        $this->updateVisit();
    }

    /**
     * @param string $view
     * @param array $parameters
     * @param Response $response
     * @return Response
     */
    public function render($view, array $parameters = array(), Response $response = null)
    {
        $response = parent::render($view, $parameters, $response);

        foreach ($this->cookies as $cookie) $response->headers->setCookie($cookie);

        return $response;
    }

    /**
     * @param \DateTimeZone $timezone
     */
    public function setTimezone(\DateTimeZone $timezone)
    {
        $this->timezone = $timezone;

        date_default_timezone_set($this->timezone->getName());
    }

    /**
     * @return bool
     */
    public function updateVisit()
    {
        $update = ($this->user instanceof AbstractUser)
            && $this->authChecker->isGranted(Role::USER)
            && $this->user->updateVisit();

        if ($update) {
            $this->manager->persist($this->user);
            $this->manager->flush();
        }

        return $update;
    }

    protected function specifyLocale()
    {
        $this->locale = $this->request->query->get(
            'locale',
            $this->session->get('main.locale', $this->request->getLocale())
        );

        if (empty($this->locale)) $this->locale = $this->container->getParameter('default_locale');

        $this->session->set('main.locale', $this->locale);
        $this->get('translator')->setLocale($this->locale);
    }
}
