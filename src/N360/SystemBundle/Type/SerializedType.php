<?php

namespace N360\SystemBundle\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

class SerializedType extends Type
{
    const NAME = 'serialized';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (empty($value)) return null;

        try {
            return serialize($value);
        }
        catch (\Exception $e) {
            throw ConversionException::conversionFailed($value, self::NAME);
        }
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (empty($value)) return null;

        try {
            return unserialize($value);
        }
        catch (\Exception $e) {
            throw ConversionException::conversionFailed($value, self::NAME);
        }
    }

    public function getName()
    {
        return self::NAME;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getClobTypeDeclarationSQL($fieldDeclaration);
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}
