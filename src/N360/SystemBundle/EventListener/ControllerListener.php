<?php

namespace N360\SystemBundle\EventListener;

use N360\SystemBundle\Controller\AbstractInitializableController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class ControllerListener
{
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller)) return;

        $controller = $controller[0];

        if ($controller instanceof AbstractInitializableController) $controller->initialize($event->getRequest());
    }
}
