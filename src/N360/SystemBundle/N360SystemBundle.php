<?php

namespace N360\SystemBundle;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use N360\SystemBundle\Type\SerializedType;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class N360SystemBundle extends Bundle
{
    public function boot()
    {
        if (!Type::hasType(SerializedType::NAME))
            Type::addType(SerializedType::NAME, 'N360\SystemBundle\Type\SerializedType');

        /** @var AbstractPlatform $platform */
        $platform = $this->container->get('database_connection')->getDatabasePlatform();

        if (!$platform->hasDoctrineTypeMappingFor(SerializedType::NAME))
            $platform->registerDoctrineTypeMapping(Type::TEXT, SerializedType::NAME);
    }
}
