<?php

//use Symfony\Component\ClassLoader\ApcClassLoader;
use Symfony\Component\HttpFoundation\Request;

$loader = require_once __DIR__ . '/../app/bootstrap.php.cache';

/*
$apcLoader = new ApcClassLoader(sha1(__FILE__), $loader);
$loader->unregister();
$apcLoader->register(true);
*/

require_once __DIR__ . '/../app/AppKernel.php';
require_once __DIR__ . '/../app/AppCache.php';

Request::enableHttpMethodParameterOverride();
$request = Request::createFromGlobals();
$domain = implode('.', array_reverse(array_slice(array_reverse(explode('.', strtolower($request->getHost()))), 0, 2)));
$kernel = new AppKernel('prod', false, $domain);
$kernel->loadClassCache();
$kernel = new AppCache($kernel);
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
